ARG BUILD_FROM=hassioaddons/base-amd64:1.3.0
FROM ${BUILD_FROM}
 
ENV LANG C.UTF-8
 
RUN apk add --no-cache git --virtual .build-deps openssl wget \ 

    && mkdir frp \
    && cd frp
    && wget https://github.com/fatedier/frp/releases/download/v${FRP_VERSION}/frp_${FRP_VERSION}_linux_arm.tar.gz \
    && tar zxvf frp_${FRP_VERSION}_linux_arm.tar.gz \
    && rm frp_${FRP_VERSION}_linux_arm.tar.gz \
    && cd frp_${FRP_VERSION}_linux_arm \
    
    && cp frpc /frp/frpc \
    && cp frpc.ini /frp/frpc.ini \
    && cd .. \
    && rm -rf frp_${FRP_VERSION}_linux_arm \
    && apk del .build-deps

COPY run.sh /
RUN chmod a+x /run.sh
 
CMD [ "/run.sh" ]
